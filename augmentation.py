from PIL import Image
import numpy as np
import cv2
import random
import glob, os
import nibabel as nib
from matplotlib import pyplot as plt
import imageio
import copy, sys



def visualize(img_list=[], show=False, output_path=None):
    if not img_list:
        print("at least one image")
        return None
    fig, axarr = plt.subplots(1, len(img_list), figsize=(6 * len(img_list), 8))

    for idx in range( len(img_list)):
        axarr[idx].imshow(img_list[idx], cmap= "gray" if np.amax(img_list[idx]) == 1 else "viridis")
    if show:
        plt.show()
    if output_path:
        fig.savefig(output_path, bbox_inches='tight')


def visualize_gif(folder):
    png_list = glob.glob(os.path.join(folder, "*.png"))
    with imageio.get_writer(os.path.join(folder, "animation.gif"), mode='I') as writer:
        for idx in range(1, len(png_list) + 1):
            print("adding number", idx)
            image = imageio.imread(os.path.join(folder, "{:03d}.png".format(idx)))
            writer.append_data(image)


def dilate_mask(mask, ks=None, ks_range=(5, 10)):
    if not ks:
        ks = random.randint(ks_range[0], ks_range[1])
    kernel = np.ones([ks, ] * 2, dtype="uint8")
    mask_dilated = cv2.dilate(np.uint8(mask) , kernel, iterations=1)
    print("mask is dilated by a kernel with a size of {}".format(ks))
    return mask_dilated


def erode_mask(mask, ks=None, ks_range=(2, 3)):
    if not ks:
        ks = random.randint(ks_range[0], ks_range[1])
    kernel = np.ones([ks, ] * 2, dtype="uint8")
    mask_eroded = cv2.erode(np.uint8(mask) , kernel, iterations=1)
    print("mask is eroded by a kernel with a size of {}".format(ks))
    return mask_eroded


def increase_brightness(img, value=30):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    h, s, v = cv2.split(hsv)

    lim = 255 - value
    v[v > lim] = 255
    v[v <= lim] += value

    final_hsv = cv2.merge((h, s, v))
    img = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)
    return img


def overlay_patch(img, patch_mask, patch_num=5, img_patch_list=None):
    pre_co = np.argwhere(patch_mask[..., 0] > 0 )
    print(pre_co.shape)
    print(np.amax(pre_co, axis=0), np.amin(pre_co, axis=0))
    co_max = np.amax(pre_co, axis=0)
    co_min = np.amin(pre_co, axis=0)
    x_len = co_max[0] - co_min[0]
    y_len = co_max[1] - co_min[1]

    x_list = random.sample(range(110, img.shape[0] - x_len), patch_num)
    y_list = random.sample(range(0, img.shape[1]- y_len ), patch_num)

    target_patch = img[co_min[0]: co_min[0] + x_len, co_min[1]: co_min[1] + y_len, :]

    print(target_patch.shape)
    counter = 0
    for x, y in zip(x_list, y_list):
        print("selected area:", "[{}:{}, {}:{}]".format(x, x + x_len, y, y + y_len))
        if img_patch_list:
            target_patch = img_patch_list[counter][co_min[0]: co_min[0] + x_len, co_min[1]: co_min[1] + y_len, :]
        img[x: x + x_len, y: y + y_len, :] = target_patch
        patch_mask[x: x + x_len, y: y + y_len, :] = 1
        counter += 1
    print("overlayed {} more targets".format(patch_num))
    return img, patch_mask


def duplicate_target(img, mask):
    pass


def create_gaussian_kernel(klen=5, sig=1.0):
    ax = np.linspace(-(klen - 1) / 2., (klen - 1) / 2., klen)
    gauss = np.exp(-0.5 * np.square(ax) / np.square(sig))
    kernel = np.outer(gauss, gauss)
    # return kernel / np.sum(kernel)
    return kernel


def gaussian_kernel_to_bool(kernel,):
    output = np.zeros(kernel.shape)
    for x in range(output.shape[0]):
        for y in range(output.shape[1]):
            output[x, y] = np.random.choice([True, False], p=[kernel[x, y], 1 - kernel[x, y]])
    output[output > 255] = 255
    return output


def apply_gaussian_kernel(img, region, kernel):
    print(region)
    img_output = copy.deepcopy(img)
    klen = int(kernel.shape[0] / 2)
    for x in range(region[0][0], region[0][1]):
        for y in range(region[1][0], region[1][1]):
            print(x, y)
            img[x, y] = 255
            img_output[x, y] = np.sum(img[x - klen: x + klen + 1, y - klen: y + klen + 1] * kernel)
            # print(img[x, y], v)
            print(img[x, y], img_output[x, y])
    return img_output


def add_artifacts_line(img, point_coord, len, kernel):
    klen = int(kernel.shape[0] / 2) 
    img_output = copy.deepcopy(img)
    for x in range(len):
        print(point_coord[0] - klen - x, point_coord[0] + klen + 1 -x, point_coord[1] - klen, point_coord[1] + klen + 1)
        img_output[point_coord[0] - klen - x: point_coord[0] + klen + 1 -x, point_coord[1] - klen: point_coord[1] + klen + 1] = img[point_coord[0] - klen - x: point_coord[0] + klen + 1 -x, point_coord[1] - klen: point_coord[1] + klen + 1] + kernel

        print(img[point_coord[0] - klen - x: point_coord[0] + klen + 1 -x, point_coord[1] - klen: point_coord[1] + klen + 1] + 255)
        print(img_output[point_coord[0] - klen - x: point_coord[0] + klen + 1 -x, point_coord[1] - klen: point_coord[1] + klen + 1], img_output[point_coord[0] - klen - x: point_coord[0] + klen + 1 -x, point_coord[1] - klen: point_coord[1] + klen + 1].shape)
    
    return img_output


def pa_augmentation():
    img_folder = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/data/photoacoustic/noisy_pencil_lead"
    mask_path = "/mnt/ceph/tco/TCO-Staff/Homes/liupeng/NeuralNetworks/nnUnet/nnUNet_raw_data_base/nnUNet_raw_data/Task504_pa/labelsTr/pa_100.nii.gz"
    output_folder = os.path.join(img_folder, "augmented/more_pencil_leads_0")
    mask = nib.load(mask_path).dataobj
    mask = np.asarray(mask)
    print(np.sum(mask == 1))
    print(mask.shape)
    img_list = glob.glob(os.path.join(img_folder, "*.png"))
    counter = 0
    for img_path in img_list:
        print(img_path)
        img = Image.open(img_path)
        print(type(img))
        img = np.asarray(img)

        for _ in range(0, 5):
            patch_num = random.randint(1, 5)
            file_idx_list = [random.randrange(0, len(img_list), 1) for i in range(patch_num)]
            img_patch_list = [np.asarray(Image.open(img_list[f_idx])) for f_idx in file_idx_list]
            
            mask_dilated = dilate_mask(mask=mask, ks=10)

            img_target_patch = img * mask_dilated
            print(img_target_patch.shape)
            print(np.sum(img_target_patch[..., 0] > 0))

            img_with_patches, new_mask = overlay_patch(img=copy.deepcopy(img), patch_mask=mask_dilated, patch_num=patch_num, img_patch_list=img_patch_list)
            new_mask = erode_mask(new_mask, ks=10)
            # visualize(img_list=[img_with_patches, new_mask[..., 0]], show=False, output_path=os.path.join(output_folder, "plots", "pa_plot_{:04d}.png".format(counter)))
            visualize(img_list=[img_with_patches, new_mask[..., 0]], show=False, output_path=None)

            cv2.imwrite(filename=os.path.join(output_folder, "pa_{:04d}.png".format(counter)), img= cv2.cvtColor(img_with_patches, cv2.COLOR_RGB2BGR))
            ni_img = nib.Nifti1Image(img_with_patches, affine=np.eye(4))
            nib.save(ni_img, os.path.join(output_folder, "niigz/img", "pa_{:04d}_0000.nii.gz".format(counter)))
            ni_gt = nib.Nifti1Image(new_mask, affine=np.eye(4))
            nib.save(ni_gt, os.path.join(output_folder, "niigz/gt", "pa_{:04d}.nii.gz".format(counter)))

            print("png:", os.path.join(output_folder, "pa_{:04d}.png".format(counter)))
            print("img:", os.path.join(output_folder, "niigz/img", "pa_{:04d}_0000.nii.gz".format(counter)))
            print("gt:", os.path.join(output_folder, "niigz/gt", "pa_{:04d}.nii.gz".format(counter)))

            counter += 1


if __name__ == "__main__":
    pa_augmentation()
